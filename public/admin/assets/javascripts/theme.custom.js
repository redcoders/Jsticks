/* Add here all your JS customizations */





function validatioinput(elemt)
    {
        var elemt_val= $('#'+elemt).val();
        if(elemt_val == '')
        {
            $('#'+elemt).parent('div').addClass('has-error').removeClass("has-success");
            $('#'+elemt).focus();
            return false;
        }
        else
        {
            $('#'+elemt).parent('div').addClass('has-success').removeClass("has-error");
            return true;
        }
    }

function validatioradio(elemt)
    {
        if (!$('input[name='+elemt+']:checked').val() ) {          
            $('#'+elemt+'_div').addClass('has-error').removeClass("has-success");
            $('#'+elemt).focus();
            return false;
        }
        else
        {
            $('#'+elemt+'_div').addClass('has-success').removeClass("has-error");
            return true;
        }
    }

function validatiomo(elemt)
    {
        var elemt_val= $('#'+elemt).val();
        if(elemt_val == '')
        {
            $('#'+elemt).parent('div').addClass('has-error').removeClass("has-success");
            $('#'+elemt).focus();
            return false;
        }
        else
        {
            if(isNaN(elemt_val))
            {
                alert("Mobile number in NUMBERS only !!");
                $('#'+elemt).parent('div').addClass('has-error').removeClass("has-success");
                $('#'+elemt).focus();
                return false;                           
            }
            else
            {
                if(elemt_val.length != 10)
                {
                    alert("Mobile number must in 10 digits only !!");
                    $('#'+elemt).parent('div').addClass('has-error').removeClass("has-success");
                    $('#'+elemt).focus();
                    return false;
                }
                else
                {
                    $('#'+elemt).parent('div').addClass('has-success').removeClass("has-error");
                    return true;
                }   
            }
            
        }
    }