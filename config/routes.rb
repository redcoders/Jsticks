Rails.application.routes.draw do
  resources :products
  resources :users
  resources :registration


  # not sure pages
  #/home/mtaj/JSticks_html_new/exp_service.html

  post 'login' => 'registration#login'
  get 'logout' => 'registration#logout'
  get 'login' => 'registration'
  get 'contact' => 'users#contact'
  post 'contact' => 'users#contact_send'
  get 'how_work' => 'users#how_work'
  get 'price' => 'users#price'
  get 'service' => 'users#service'
  get 'hardware_spec_jcreate' => 'users#hardware_spec_jcreate'
  get 'jstore' => 'users#jstore'
  get 'jstore/:catid' => 'users#jstore'
  get 'jstore/:catid/:subcatid' => 'users#jstore'
  post 'jstore' => 'users#jstore'
  get 'product/:proid' => 'users#product'

  get 'cart' => 'users#cart'
  get 'remove_cart_item' => 'users#remove_cart_item'
  post 'cart' => 'users#cartaction'


  get 'select_user_jshare' => 'users#select_user_jshare'
  post 'jshare_upload' => 'users#jshare_upload'

  get 'jsolo' => 'users#jsolo'
  get 'billing' => 'users#billing'
  get 'cart_jshare' => 'users#cart_jshare'
  get 'all_soft_jstore' => 'users#all_soft_jstore'
  get 'plan_jstore' => 'users#plan_jstore'
  get 'hardware_spec' => 'users#hardware_spec'
  get 'select_cloud' => 'users#select_cloud'


  post 'add_to_cart' => 'jshare#add_to_cart'
  post 'remove_from_cart' => 'jshare#remove_from_cart'
  post 'change_billing' => 'jshare#change_billing'
  post 'get_sub_categories' => 'jshare#get_sub_categories'
  post 'get_category_products' => 'jshare#get_category_products'
  post 'get_sub_category_products' => 'jshare#get_sub_category_products'
  post 'search_products' => 'jshare#search_products'

  get 'faq' => 'users#faq'
  get 'release_notes' => 'users#release_notes'
  get 'hiring' => 'users#hiring'
  get 'product_support' => 'users#product_support'
  get 'knowledge_base' => 'users#knowledge_base'

  get 'admin/packages' => 'admin#packages'
  get 'admin/packages/add' => 'admin#packages_form'
  get 'admin/packages/edit/:id' => 'admin#packages_set'
  get 'admin/packages/delete/:id' => 'admin#packages_delete'
  post 'admin/packages' => 'admin#packages_save'


  get 'admin/subscriber' => 'admin#subscriber'
  get 'admin/subscriber/edit/:id' => 'admin#subscriber_set'
  get 'admin/subscriber/delete/:id' => 'admin#subscriber_delete'
  post 'admin/subscriber' => 'admin#subscriber_save'


  get 'admin/services' => 'admin#services'
  get 'admin/services/add' => 'admin#service_form'
  get 'admin/services/edit/:id' => 'admin#service_set'
  get 'admin/services/delete/:id' => 'admin#service_delete'
  post 'admin/services' => 'admin#service_save'


  get 'admin/homeslider' => 'admin#homeslider'
  get 'admin/homeslider/add' => 'admin#homeslider_form'
  get 'admin/homeslider/edit/:id' => 'admin#homeslider_set'
  get 'admin/homeslider/delete/:id' => 'admin#homeslider_delete'
  post 'admin/homeslider' => 'admin#homeslider_save'



  get 'admin/software_n_games' => 'admin#software_n_games'
  get 'admin/software_n_games' => 'admin#software_n_games'
  get 'admin/software_n_games/add' => 'admin#software_n_games_form'
  get 'admin/software_n_games/edit/:id' => 'admin#software_n_games_set'
  get 'admin/software_n_games/delete/:id' => 'admin#software_n_games_delete'
  post 'admin/software_n_games' => 'admin#software_n_games_save'




  get 'admin/softwarecategory' => 'admin#softwarecategory'
  get 'admin/softwarecategory/add' => 'admin#softwarecategory_form'
  get 'admin/softwarecategory/edit/:id' => 'admin#softwarecategory_set'
  get 'admin/softwarecategory/delete/:id' => 'admin#softwarecategory_delete'
  post 'admin/softwarecategory' => 'admin#softwarecategory_save'

  get 'admin/softwaresubcategory' => 'admin#softwaresubcategory'
  get 'admin/softwaresubcategory/add' => 'admin#softwaresubcategory_form'
  get 'admin/softwaresubcategory/edit/:id' => 'admin#softwaresubcategory_set'
  get 'admin/softwaresubcategory/delete/:id' => 'admin#softwaresubcategory_delete'
  post 'admin/softwaresubcategory' => 'admin#softwaresubcategory_save'


  get 'admin/inquiry' => 'admin#inquiry'
  get 'admin/inquiry/delete/:id' => 'admin#inquiry_delete'
  get 'admin/inquiry/edit/:id' => 'admin#inquiry_set'
  post 'admin/inquiry' => 'admin#inquiry_save'

  get 'admin/site_setting_main' => 'admin#site_setting_main'
  get 'admin/site_setting_service' => 'admin#site_setting_service'
  get 'admin/site_setting_pricing' => 'admin#site_setting_pricing'
  get 'admin/site_setting_hiw' => 'admin#site_setting_hiw'
  get 'admin/site_setting_home' => 'admin#site_setting_home'

  post 'admin/site_setting_main' => 'admin#update_site_setting_main'
  post 'admin/site_setting_service' => 'admin#update_site_setting_service'
  post 'admin/site_setting_pricing' => 'admin#update_site_setting_pricing'
  post 'admin/site_setting_hiw' => 'admin#update_site_setting_hiw'
  post 'admin/site_setting_home' => 'admin#update_site_setting_home'

  get 'admin/cloud_service' => 'admin#cloud_service'
  get 'admin/cloud_service/add' => 'admin#cloud_service_form'
  get 'admin/cloud_service/edit/:id' => 'admin#cloud_service_set'
  get 'admin/cloud_service/delete/:id' => 'admin#cloud_service_delete'
  post 'admin/cloud_service' => 'admin#cloud_service_save'



  get 'admin/cloud_os' => 'admin#cloud_os'
  get 'admin/cloud_os/add' => 'admin#cloud_os_form'
  get 'admin/cloud_os/edit/:id' => 'admin#cloud_os_set'
  get 'admin/cloud_os/delete/:id' => 'admin#cloud_os_delete'
  post 'admin/cloud_os' => 'admin#cloud_os_save'



  get 'admin/cloud_ram' => 'admin#cloud_ram'
  get 'admin/cloud_ram/add' => 'admin#cloud_ram_form'
  get 'admin/cloud_ram/edit/:id' => 'admin#cloud_ram_set'
  get 'admin/cloud_ram/delete/:id' => 'admin#cloud_ram_delete'
  post 'admin/cloud_ram' => 'admin#cloud_ram_save'


  get 'admin/cloud_harddisk' => 'admin#cloud_harddisk'
  get 'admin/cloud_harddisk/add' => 'admin#cloud_harddisk_form'
  get 'admin/cloud_harddisk/edit/:id' => 'admin#cloud_harddisk_set'
  get 'admin/cloud_harddisk/delete/:id' => 'admin#cloud_harddisk_delete'
  post 'admin/cloud_harddisk' => 'admin#cloud_harddisk_save'



  get 'admin/adminuser' => 'admin#adminuser'
  get 'admin/adminuser/add' => 'admin#adminuser_form'
  get 'admin/adminuser/edit/:id' => 'admin#adminuser_set'
  get 'admin/adminuser/delete/:id' => 'admin#adminuser_delete'
  post 'admin/adminuser' => 'admin#adminuser_save'


  get 'admin/faq' => 'admin#faq'
  post 'admin/faq' => 'admin#faq'
  get 'admin/release_notes' => 'admin#release_notes'
  post 'admin/release_notes' => 'admin#release_notes'
  get 'admin/hiring' => 'admin#hiring'
  post 'admin/hiring' => 'admin#hiring'
  get 'admin/product_support' => 'admin#product_support'
  post 'admin/product_support' => 'admin#product_support'
  get 'admin/knowledge_base' => 'admin#knowledge_base'
  post 'admin/knowledge_base' => 'admin#knowledge_base'


  resources :admin

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end


root 'users#index'
#root 'products#index'
#get 'contact/' => 'contact#view'

end
