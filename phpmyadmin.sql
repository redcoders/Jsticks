-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2017 at 05:16 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmyadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `package` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `product_id`, `package`, `created_at`, `updated_at`) VALUES
(10, 3, 1, 'P1, 5, 15', '2017-04-19 10:26:45', '2017-04-19 10:26:45'),
(13, 3, 13, 'package 1, 33, 40', '2017-04-21 09:02:52', '2017-04-21 09:02:52'),
(15, 3, 1, 'P1, 5, 15', '2017-04-21 18:15:00', '2017-04-21 18:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `cloud_harddisks`
--

CREATE TABLE `cloud_harddisks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cloud_harddisks`
--

INSERT INTO `cloud_harddisks` (`id`, `title`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, '01', '0', 'inactive', '2017-04-14 01:05:36', '2017-04-14 01:05:36'),
(2, '2001', '99', 'active', '2017-04-14 01:05:36', '2017-04-14 01:05:36'),
(3, '8001', '299', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(4, '10241', '599', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(5, '20481', '999', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `cloud_os`
--

CREATE TABLE `cloud_os` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cloud_os`
--

INSERT INTO `cloud_os` (`id`, `title`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Linux1', '80', 'active', '2017-04-14 01:05:36', '2017-04-14 01:05:36'),
(2, 'Mac2', '90', 'active', '2017-04-14 01:05:36', '2017-04-14 01:05:36');

-- --------------------------------------------------------

--
-- Table structure for table `cloud_rams`
--

CREATE TABLE `cloud_rams` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cloud_rams`
--

INSERT INTO `cloud_rams` (`id`, `title`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, '0', '80', 'inactive', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(2, '2', '20', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(3, '4', '80', 'inactive', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(4, '6', '9', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `cloud_services`
--

CREATE TABLE `cloud_services` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cloud_services`
--

INSERT INTO `cloud_services` (`id`, `title`, `price`, `logo`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Google cloud', NULL, 'Icon.png', 'inactive', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(2, 'Azure', NULL, 'Icon.png', 'inactive', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(3, 'Vmare', NULL, 'Icon.png', 'inactive', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(4, 'Amazon', NULL, 'aws-253d5f49f8.png', 'active', '2017-04-14 01:05:37', '2017-04-14 01:05:37'),
(5, 'Extra', NULL, 'prev-e90585610a.png', 'inactive', '2017-04-14 01:05:38', '2017-04-14 19:16:09'),
(6, 'Extra2222', NULL, 'aws-253d5f49f8.png', 'inactive', '2017-04-14 11:11:18', '2017-04-14 11:11:18'),
(7, 'Azure', NULL, 'Microsoft-Azure-7cc2685a04.png', 'active', '2017-04-14 11:20:02', '2017-04-14 11:20:02'),
(9, 'Vmare', NULL, 'vmware-logo-c2d4d9125a.png', 'active', '2017-04-14 11:21:29', '2017-04-14 11:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `id` int(11) NOT NULL,
  `faq` text COLLATE utf8_unicode_ci,
  `release_notes` text COLLATE utf8_unicode_ci,
  `hiring` text COLLATE utf8_unicode_ci,
  `product_support` text COLLATE utf8_unicode_ci,
  `knowledge_base` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `faq`, `release_notes`, `hiring`, `product_support`, `knowledge_base`, `created_at`, `updated_at`) VALUES
(1, '<p>My test FAQ</p>\r\n', '<p>release notes here..</p>\r\n', '<p>we are&nbsp;Hiring !!</p>\r\n', '<p>product support here !!</p>\r\n', '<p>knowledge base here !!</p>\r\n', '2017-04-14 01:05:40', '2017-04-19 10:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `homesliders`
--

CREATE TABLE `homesliders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag_line` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `read_more` text COLLATE utf8_unicode_ci NOT NULL,
  `start_now` text COLLATE utf8_unicode_ci NOT NULL,
  `price_per_month` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `homesliders`
--

INSERT INTO `homesliders` (`id`, `title`, `tag_line`, `description`, `read_more`, `start_now`, `price_per_month`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'WELCOME TO JSticks1', 'WE GIVE BEST WEB CLOUD SERVICE FOR YOUR USE', 'Duis autem veleum iriure dolor in hendrerit in vulput atevelit ess \\r\\niriure dolor in hendrerit in vulput atevelit ess veleum iriure', 'http://localhost:3000/service', 'http://localhost:3000/service', '40', 'active', 'slider-image3.png', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(2, 'WELCOME TO JSticks2', 'WE GIVE BEST WEB CLOUD SERVICE FOR YOUR USE', 'Duis autem veleum iriure dolor in hendrerit in vulput atevelit ess \\r\\niriure dolor in hendrerit in vulput atevelit ess veleum iriure', 'http://localhost:3000/service', 'http://localhost:3000/service', '40', 'active', 'slider-image3.png', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(3, 'WELCOME TO JSticks3', 'WE GIVE BEST WEB CLOUD SERVICE FOR YOUR USE', 'Duis autem veleum iriure dolor in hendrerit in vulput atevelit ess \\r\\niriure dolor in hendrerit in vulput atevelit ess veleum iriure', 'http://localhost:3000/service', 'http://localhost:3000/service', '40', 'active', 'slider-image3.png', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(4, 'Extra slider', 'WE GIVE BEST WEB CLOUD SERVICE FOR YOUR USE', 'Duis autem veleum iriure dolor in hendrerit in vulput atevelit ess \\r\\niriure dolor in hendrerit in vulput atevelit ess veleum iriure', 'http://localhost:3000/service', 'http://localhost:3000/service', '40', 'inactive', 'slider-image3.png', '2017-04-14 01:05:38', '2017-04-14 01:05:38');

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastname` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `reply` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inquiries`
--

INSERT INTO `inquiries` (`id`, `firstname`, `lastname`, `email`, `phone`, `message`, `reply`, `status`, `created_at`, `updated_at`) VALUES
(1, 'keyur', 'patel', 'k@k.com', '9898098080', 'this is test msg....', 'copy...\\r\\nget back to u ASAP.....', 'replied', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(2, 'jhom', 'smith', 'jhon@smith.com', '8523697418', 'this is test msg....!!', '', 'new', '2017-04-14 01:05:38', '2017-04-14 01:05:38');

-- --------------------------------------------------------

--
-- Table structure for table `jshare_carts`
--

CREATE TABLE `jshare_carts` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_package` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jshare_carts`
--

INSERT INTO `jshare_carts` (`id`, `group_id`, `user_id`, `product_id`, `product_package`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 5, 'P1, 5, 15', '2017-04-14 08:50:39', '2017-04-14 08:50:39'),
(4, 2, 3, 2, 'P1, 5, 15', '2017-04-15 11:25:22', '2017-04-15 11:25:22'),
(5, 2, 4, 2, 'P1, 5, 15', '2017-04-15 11:25:22', '2017-04-15 11:25:22'),
(6, 2, 3, 6, 'P1, 5, 15', '2017-04-15 11:25:26', '2017-04-15 11:25:26'),
(8, 2, 3, 1, 'P1, 5, 15', '2017-04-15 11:25:30', '2017-04-15 11:25:30'),
(10, 2, 4, 3, 'P1, 5, 15', '2017-04-15 11:25:43', '2017-04-15 11:25:43'),
(11, 2, 4, 7, 'P1, 5, 15', '2017-04-15 11:25:46', '2017-04-15 11:25:46'),
(12, 3, 5, 2, 'P1, 5, 15', '2017-04-15 11:27:09', '2017-04-15 11:27:09'),
(13, 3, 6, 3, 'P1, 5, 15', '2017-04-15 11:27:11', '2017-04-15 11:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `jshare_groups`
--

CREATE TABLE `jshare_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jshare_groups`
--

INSERT INTO `jshare_groups` (`id`, `group_name`, `group_password`, `email`, `created_at`, `updated_at`) VALUES
(1, 'my group', '123456', 'mygroup@gmail.com', '2017-04-14 08:50:04', '2017-04-14 08:50:04'),
(2, 'abc group', 'abc', 'ab@gmiail.com', '2017-04-15 11:25:16', '2017-04-15 11:25:16'),
(3, 'group name', 'abc', 'ab@gmiail.com', '2017-04-15 11:27:02', '2017-04-15 11:27:02'),
(4, 'kp', 'kp', 'k@k.com', '2017-04-19 16:00:04', '2017-04-19 16:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `jshare_users`
--

CREATE TABLE `jshare_users` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jshare_users`
--

INSERT INTO `jshare_users` (`id`, `group_id`, `first_name`, `last_name`, `email`, `billing`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mark', 'Jacob', 'jstick@mail.com', 'self', '2017-04-14 08:50:04', '2017-04-14 08:50:04'),
(2, 1, 'Jhon', 'Doe', 'jd@mail.com', 'self', '2017-04-14 08:50:04', '2017-04-14 08:50:04'),
(3, 2, 'Mark', 'Jacob', 'jstick@mail.com', 'billed', '2017-04-15 11:25:16', '2017-04-15 11:25:48'),
(4, 2, 'Jhon', 'Doe', 'jd@mail.com', 'billed', '2017-04-15 11:25:16', '2017-04-15 11:25:49'),
(5, 3, 'Mark', 'Jacob', 'jstick@mail.com', 'billed', '2017-04-15 11:27:02', '2017-04-15 11:27:07'),
(6, 3, 'Jhon', 'Doe', 'jd@mail.com', 'self', '2017-04-15 11:27:02', '2017-04-15 11:27:02'),
(7, 4, NULL, NULL, NULL, 'self', '2017-04-19 16:00:04', '2017-04-19 16:00:04'),
(8, 4, NULL, NULL, NULL, 'self', '2017-04-19 16:00:04', '2017-04-19 16:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `hours_per_day` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gb_storage` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price_per_month` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `premium` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `description`, `hours_per_day`, `gb_storage`, `price_per_month`, `status`, `premium`, `created_at`, `updated_at`) VALUES
(1, 'PERSONAL PLAN 1', 'Priority Support', '3', '100', '10', 'active', 'no', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(2, 'PERSONAL PLAN', 'Priority Support', '6', '200', '20', 'active', 'no', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(3, 'PROFESSIONAL PLAN', 'Priority Support', '9', '500', '40', 'active', 'yes', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(4, 'ENTERPRISE PLAN', 'Priority Support', '12', '600', '60', 'active', 'no', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(5, 'Extra PLAN', 'Priority Support', '1500', '5000', '90', 'inactive', 'no', '2017-04-14 01:05:38', '2017-04-14 01:05:38');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `published` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20170224172747'),
('20170225130928'),
('20170410191159'),
('20170410192436'),
('20170410192551'),
('20170410192651'),
('20170410192740'),
('20170410192824'),
('20170410192952'),
('20170410193059'),
('20170410193746'),
('20170410194430'),
('20170410195217'),
('20170413111439'),
('20170413172653'),
('20170413172719'),
('20170413172729'),
('20170413234229');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `premium` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `description`, `image`, `status`, `premium`, `created_at`, `updated_at`) VALUES
(1, 'Ultra Low Latency1', 'Our proprietary compression algorithms deliver you the best quality graphics with the least amount of latency possible.', 'choose_icon3.png', 'active', 'no', '2017-04-14 01:05:38', '2017-04-24 11:44:29'),
(2, 'High Performance Hardware', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'yes', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(3, 'Backup Of Session', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'no', '2017-04-14 01:05:38', '2017-04-14 01:05:38'),
(4, 'Lightning Internet Speed', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'yes', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(5, 'Secured Cloud', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat\\r\\n\\r\\n', 'choose_icon3.png', 'active', 'yes', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(6, 'Run Any Software With Any Mod', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'yes', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(7, 'Windows', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'no', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(8, 'Cloud Storage', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'no', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(9, 'Always Up-To-Date Tech', 'Duis autem vel eum iriure dolor in hen vulputate velit esse molestie conse vel illum dolore eu feugiat', 'choose_icon3.png', 'active', 'yes', '2017-04-14 01:05:39', '2017-04-14 01:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `footer_text` text COLLATE utf8_unicode_ci NOT NULL,
  `footer_copyright` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gmap_longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gmap_latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logo` varchar(6000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service_page_title` text COLLATE utf8_unicode_ci NOT NULL,
  `service_page_description` text COLLATE utf8_unicode_ci NOT NULL,
  `service_page_opt_in_text` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_page_title` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step3_title` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step2_title` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step1_title` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step3_description` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step2_description` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_step1_description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_page_intro_title` text COLLATE utf8_unicode_ci NOT NULL,
  `price_page_intro_description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_page_features_title` text COLLATE utf8_unicode_ci NOT NULL,
  `price_page_features_description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_page_you_get_title` text COLLATE utf8_unicode_ci NOT NULL,
  `price_page_you_get_description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_page_start_now_title` text COLLATE utf8_unicode_ci NOT NULL,
  `price_page_start_now_description` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `price_page_title` text COLLATE utf8_unicode_ci NOT NULL,
  `price_page_description` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `footer_text`, `footer_copyright`, `contact_email`, `contact_number`, `gmap_longitude`, `gmap_latitude`, `logo`, `service_page_title`, `service_page_description`, `service_page_opt_in_text`, `how_work_page_title`, `how_work_step3_title`, `how_work_step2_title`, `how_work_step1_title`, `how_work_step3_description`, `how_work_step2_description`, `how_work_step1_description`, `home_page_intro_title`, `price_page_intro_description`, `home_page_features_title`, `price_page_features_description`, `home_page_you_get_title`, `price_page_you_get_description`, `home_page_start_now_title`, `price_page_start_now_description`, `contact_address`, `created_at`, `updated_at`, `price_page_title`, `price_page_description`, `avatar`) VALUES
(1, 'jstick123', 'This is footer about us autem vel eum iriure dolores in hendrerit in vulputate velit esse autem vel eum iriure dolore.', '© Copyright 2017 JSticks. All Right Reserved1', 'test@k.com', '9898098981', '41.901630', '12.460245', 'logo.png', 'Benefits Appreciate1', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'We give always awesome service.2', 'How It Works1', 'Finish iriure dolor4', 'Your actions via mouse, keyboard.2', 'Access JSticks Via Your Devices1', 'Duis autem vel eum iriure dolor3', 'Duis autem vel eum iriure dolor3', 'Duis autem vel eum iriure dolor1', 'What Is JSticks? ', 'JSticks gives you a high performance gaming PC in the cloud. Access your SkyComputer from any device and play with ultra low latency and high performance graphics.1', 'Why Choose Us?2', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'What You Can Get1', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'We give always best cloud service for your use.2', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molesti consequat, vel illum dolore eu feugiat nulla faciisis at vero eros et accum dignissim qui blandit praesent luptatum.2', 'wages, Newada, USA', '2017-04-14 01:05:39', '2017-04-16 11:48:00', 'Choose Pricing Plans1', 'Duis islut aliquip ex ea commodo consequat. Duis autem1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `softwarecategories`
--

CREATE TABLE `softwarecategories` (
  `id` int(11) NOT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `softwarecategories`
--

INSERT INTO `softwarecategories` (`id`, `parent`, `title`, `icon`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, '0', 'Software', 'icofont-server', 'active', 'slider-image3.png', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(2, '0', 'Books', 'icofont-book-alt', 'active', 'slider-image3.png', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(3, '0', 'Devices', 'icofont-contrast', 'active', 'slider-image3.png', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(4, '0', 'Magazine', 'icofont-read-book', 'active', 'slider-image3.png', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(5, '0', 'Games', 'icofont-kids-scooter', 'active', 'slider-image3.png', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(6, '2', 'Beauty', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(7, '5', 'Art & Design', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(8, '2', 'Business', 'icofont-kids-scooter', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(9, '1', 'Comics', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(10, '1', 'Communication', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(11, '2', 'Dating', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(12, '3', 'Education', 'icofont-server', 'active', '', '2017-04-14 01:05:39', '2017-04-14 01:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `softwares`
--

CREATE TABLE `softwares` (
  `id` int(11) NOT NULL,
  `cat_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_cat_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isnew` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `istop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_hourly` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_monthly` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_yearly` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `softwares`
--

INSERT INTO `softwares` (`id`, `cat_id`, `sub_cat_id`, `title`, `description`, `image`, `isnew`, `istop`, `slider`, `price_hourly`, `price_monthly`, `price_yearly`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', '6', 'Adobe Illustrator zi', 'this is product 1  details', 'angry_birds.jpg', 'active', 'active', 'active', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(2, '5', '8', 'Adobe Illustrator i', 'this is product 2  details', 'angry_birds.jpg', 'active', 'active', 'active', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:39', '2017-04-14 01:05:39'),
(3, '2', '6', 'Sublime', 'tthis is testing.', 'angry_birds.jpg', 'active', 'inactive', 'active', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(4, '2', '6', 'Adobe Illustrator', 'On-the-go photo editing was never so fun, fast, and cool. Touch your way to better-looking pictures with automatic fixes and filters. Get your pictures to pop! And after sharing, you’ll be the the talk of your friends. ', 'program-c374f0d06b.png', 'active', 'active', 'inactive', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:40', '2017-04-19 10:30:01'),
(5, '2', '6', 'Angry Bird', 'angry_bird game desc...', 'angry_birds.jpg', 'active', 'active', 'active', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(6, '2', '6', 'COC', 'desc...', 'angry_birds.jpg', 'active', 'active', 'inactive', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(7, '2', '6', 'Adobe Photoshop', 'Adobe Photoshop desc...', 'angry_birds.jpg', 'active', 'active', 'active', 'P1, 5, 15', 'P2, 10, 10', 'P3, 15, 5', 'active', '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(13, '3', '12', 'Education books 1', 'test', 'fifa-c549abef99.jpg', 'active', 'inactive', 'inactive', 'package 1, 33, 40', 'package 2, 100, 30', 'package 3, 20, 33', 'active', '2017-04-16 11:50:18', '2017-04-16 13:26:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `country`, `district`, `image`, `user_type`, `status`, `login_time`, `created_at`, `updated_at`) VALUES
(1, 'keyur1', 'patel1', 'k@k.com1', 'k', '8888888888', 'india', 'Guj', 'user.png', 'subscriber', 'active', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(2, 'keyur', 'patel', 'e', 'e', '9898098980', 'india', 'London', 'user.png', 'subscriber', 'active', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(3, 'k', 'k', 'k@k.com', 'k', '9898098980', 'usa', 'Wagas', 'user.png', 'admin', 'active', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(4, 'sdfsdf', 'sdf', 'sdf', 'sdfsdf', '9898098980', 'uk', 'London', 'user.png', 'subscriber', 'new', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(5, 'jignesh', 'p', 'j@j.com', 'j@j.com', '9898098080', 'usa', 'Wagas', 'user.png', 'subscriber', 'new', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(6, 'q', 'q', 'q', 't', '9898098980', 'india', 'Wagas', 'user.png', 'subscriber', 'new', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40'),
(7, 'jhon', 'smith', 'j@j.com', 'j', '9898798986', '', '', 'user.png', 'admin', 'inactive', NULL, '2017-04-14 01:05:40', '2017-04-14 01:05:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cloud_harddisks`
--
ALTER TABLE `cloud_harddisks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cloud_os`
--
ALTER TABLE `cloud_os`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cloud_rams`
--
ALTER TABLE `cloud_rams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cloud_services`
--
ALTER TABLE `cloud_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homesliders`
--
ALTER TABLE `homesliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jshare_carts`
--
ALTER TABLE `jshare_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jshare_groups`
--
ALTER TABLE `jshare_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jshare_users`
--
ALTER TABLE `jshare_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schema_migrations`
--
ALTER TABLE `schema_migrations`
  ADD UNIQUE KEY `unique_schema_migrations` (`version`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `softwarecategories`
--
ALTER TABLE `softwarecategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `softwares`
--
ALTER TABLE `softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cloud_harddisks`
--
ALTER TABLE `cloud_harddisks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cloud_os`
--
ALTER TABLE `cloud_os`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cloud_rams`
--
ALTER TABLE `cloud_rams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cloud_services`
--
ALTER TABLE `cloud_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `homesliders`
--
ALTER TABLE `homesliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jshare_carts`
--
ALTER TABLE `jshare_carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `jshare_groups`
--
ALTER TABLE `jshare_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jshare_users`
--
ALTER TABLE `jshare_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `softwarecategories`
--
ALTER TABLE `softwarecategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `softwares`
--
ALTER TABLE `softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
