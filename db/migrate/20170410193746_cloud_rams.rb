class CloudRams < ActiveRecord::Migration
  def change
  	create_table :cloud_rams do |t|
		t.string :title
	    t.string :price
	    t.string :status
      	t.timestamps null: false
    end
  end
end
