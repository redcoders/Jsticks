class Inquiries < ActiveRecord::Migration
  def change
	  create_table "inquiries", force: :cascade do |t|
	    t.string   "firstname",  limit: 255,   default: "",    null: false
	    t.text     "lastname",   limit: 65535,                 null: false
	    t.string   "email",      limit: 255,   default: "",    null: false
	    t.string   "phone",      limit: 255,   default: "",    null: false
	    t.text     "message",    limit: 65535,                 null: false
	    t.text     "reply",      limit: 65535
	    t.string   "status",     limit: 255,   default: "new", null: false
	    t.datetime "created_at",                               null: false
	    t.datetime "updated_at",                               null: false
	  end
  end
end
