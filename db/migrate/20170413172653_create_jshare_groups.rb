class CreateJshareGroups < ActiveRecord::Migration
    def change
        create_table :jshare_groups do |t|
            t.string :group_name
            t.string :group_password
            t.string :email

            t.timestamps null: false
        end
    end
end
