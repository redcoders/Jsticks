class CreateJshareUsers < ActiveRecord::Migration
    def change
        create_table :jshare_users do |t|
            t.integer :group_id, unique: true, null: false
            t.string :first_name
            t.string :last_name
            t.string :email
            t.string :billing

            t.timestamps null: false
        end
    end
end
