class Softwares < ActiveRecord::Migration
  def change
  	create_table "softwares", force: :cascade do |t|
      t.string :cat_id
      t.string :sub_cat_id
      t.string :title
      t.string :description
      t.string :image
      t.string :isnew
      t.string :istop
      t.string :slider
      t.string :price_hourly
      t.string :price_monthly
      t.string :price_yearly
      t.string :status
      t.timestamps null: false
    end
  end
end
