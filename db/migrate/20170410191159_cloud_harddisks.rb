class CloudHarddisks < ActiveRecord::Migration
  def change
  	create_table :cloud_harddisks do |t|
		t.string :title
	    t.string :price
	    t.string :status
      	t.timestamps null: false
    end
  end
end
