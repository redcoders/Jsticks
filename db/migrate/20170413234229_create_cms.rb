class CreateCms < ActiveRecord::Migration
  def change
    create_table :cms do |t|
        t.text :faq
        t.text :release_notes
        t.text :hiring
        t.text :product_support
        t.text :knowledge_base

      t.timestamps null: false
    end
  end
end
