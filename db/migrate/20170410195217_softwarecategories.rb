class Softwarecategories < ActiveRecord::Migration
  def change
  	create_table "softwarecategories", force: :cascade do |t|
	    t.string   "parent",     limit: 255,                      null: false
	    t.string   "title",      limit: 255, default: "",         null: false
	    t.string   "icon",       limit: 255, default: "",         null: false
	    t.string   "status",     limit: 255, default: "inactive", null: false
	    t.string   "image",      limit: 255, default: "",         null: false
	    t.datetime "created_at",                                  null: false
	    t.datetime "updated_at",                                  null: false
	  end
  end
end
