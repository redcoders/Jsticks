class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
	    t.string   :first_name
	    t.string   :last_name
	    t.string   :email
	    t.string   :password
	    t.string   :phone
	    t.string   :country
	    t.string   :district
	    t.string   :image
	    t.string   :user_type
	    t.string   :status
	    t.datetime :login_time

        t.timestamps null: false
    end
  end
end
