class CreateJshareCarts < ActiveRecord::Migration
    def change
        create_table :jshare_carts do |t|
            t.integer :group_id, unique: true, null: false
            t.integer :user_id, unique: true, null: false
            t.integer :product_id, unique: true, null: false
            t.string :product_package

            t.timestamps null: false
        end
    end
end
