class CloudServices < ActiveRecord::Migration
  def change
  	create_table :cloud_services do |t|
		t.string :title
	    t.string :price
        t.string :logo
	    t.string :status
        
      	t.timestamps null: false
    end
  end
end
