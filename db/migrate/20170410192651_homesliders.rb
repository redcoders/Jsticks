class Homesliders < ActiveRecord::Migration
  def change
  	create_table "homesliders", force: :cascade do |t|
	    t.string   "title",           limit: 255,   default: "",         null: false
	    t.text     "tag_line",        limit: 65535,                      null: false
	    t.text     "description",     limit: 65535,                      null: false
	    t.text     "read_more",       limit: 65535,                      null: false
	    t.text     "start_now",       limit: 65535,                      null: false
	    t.string   "price_per_month", limit: 255,   default: "",         null: false
	    t.string   "status",          limit: 255,   default: "inactive", null: false
	    t.string   "image",           limit: 255,   default: "",         null: false
	    t.datetime "created_at",                                         null: false
	    t.datetime "updated_at",                                         null: false
	  end
  end
end
