class Services < ActiveRecord::Migration
  def change
  	create_table "services", force: :cascade do |t|
	    t.string   "title",       limit: 255,   default: "",         null: false
	    t.text     "description", limit: 65535,                      null: false
	    t.string   "image",       limit: 255,   default: "",         null: false
	    t.string   "status",      limit: 255,   default: "inactive", null: false
	    t.string   "premium",     limit: 255,   default: "",         null: false
	    t.datetime "created_at",                                     null: false
	    t.datetime "updated_at",                                     null: false
	end
  end
end
