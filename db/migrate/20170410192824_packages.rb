class Packages < ActiveRecord::Migration
  def change
  	create_table "packages", force: :cascade do |t|
    t.string   "title",           limit: 255,   default: "",         null: false
    t.text     "description",     limit: 65535,                      null: false
    t.string   "hours_per_day",   limit: 255,   default: "",         null: false
    t.string   "gb_storage",      limit: 255,   default: "",         null: false
    t.string   "price_per_month", limit: 255,   default: "",         null: false
    t.string   "status",          limit: 255,   default: "inactive", null: false
    t.string   "premium",         limit: 255,   default: "",         null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end
  end
end
