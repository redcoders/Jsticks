class CloudOs < ActiveRecord::Migration
  def change
  	create_table :cloud_os do |t|
		t.string :title
	    t.string :price
	    t.string :status
      	t.timestamps null: false
    end
  end
end
