class SiteSettings < ActiveRecord::Migration
  def change
  	create_table "site_settings", force: :cascade do |t|
	    t.string   "site_name",                        limit: 255,   default: "", null: false
	    t.text     "footer_text",                      limit: 65535,              null: false
	    t.text     "footer_copyright",                 limit: 65535,              null: false
	    t.string   "contact_email",                    limit: 255,   default: "", null: false
	    t.string   "contact_number",                   limit: 255,   default: "", null: false
	    t.string   "gmap_longitude",                   limit: 255,   default: "", null: false
	    t.string   "gmap_latitude",                    limit: 255,   default: "", null: false
	    t.string   "logo",                             limit: 6000,  default: "", null: false
	    t.text     "service_page_title",               limit: 65535,              null: false
	    t.text     "service_page_description",         limit: 65535,              null: false
	    t.text     "service_page_opt_in_text",         limit: 65535,              null: false
	    t.text     "how_work_page_title",              limit: 65535,              null: false
	    t.text     "how_work_step3_title",             limit: 65535,              null: false
	    t.text     "how_work_step2_title",             limit: 65535,              null: false
	    t.text     "how_work_step1_title",             limit: 65535,              null: false
	    t.text     "how_work_step3_description",       limit: 65535,              null: false
	    t.text     "how_work_step2_description",       limit: 65535,              null: false
	    t.text     "how_work_step1_description",       limit: 65535,              null: false
	    t.text     "home_page_intro_title",            limit: 65535,              null: false
	    t.text     "price_page_intro_description",     limit: 65535,              null: false
	    t.text     "home_page_features_title",         limit: 65535,              null: false
	    t.text     "price_page_features_description",  limit: 65535,              null: false
	    t.text     "home_page_you_get_title",          limit: 65535,              null: false
	    t.text     "price_page_you_get_description",   limit: 65535,              null: false
	    t.text     "home_page_start_now_title",        limit: 65535,              null: false
	    t.text     "price_page_start_now_description", limit: 65535,              null: false
	    t.text     "contact_address",                  limit: 65535,              null: false
	    t.datetime "created_at",                                                  null: false
	    t.datetime "updated_at",                                                  null: false
	    t.text     "price_page_title",                 limit: 65535,              null: false
	    t.text     "price_page_description",           limit: 65535,              null: false
	    t.string   "avatar",                           limit: 255
	  end
  end
end
