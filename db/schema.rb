# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170413234229) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "carts", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "product_id", null: false
    t.string   "package",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cloud_harddisks", force: :cascade do |t|
    t.string   "title"
    t.string   "price"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cloud_os", force: :cascade do |t|
    t.string   "title"
    t.string   "price"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cloud_rams", force: :cascade do |t|
    t.string   "title"
    t.string   "price"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cloud_services", force: :cascade do |t|
    t.string   "title"
    t.string   "price"
    t.string   "logo"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cms", force: :cascade do |t|
    t.text     "faq"
    t.text     "release_notes"
    t.text     "hiring"
    t.text     "product_support"
    t.text     "knowledge_base"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "homesliders", force: :cascade do |t|
    t.string   "title",           limit: 255, default: "",         null: false
    t.text     "tag_line",                                         null: false
    t.text     "description",                                      null: false
    t.text     "read_more",                                        null: false
    t.text     "start_now",                                        null: false
    t.string   "price_per_month", limit: 255, default: "",         null: false
    t.string   "status",          limit: 255, default: "inactive", null: false
    t.string   "image",           limit: 255, default: "",         null: false
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "inquiries", force: :cascade do |t|
    t.string   "firstname",  limit: 255, default: "",    null: false
    t.text     "lastname",                               null: false
    t.string   "email",      limit: 255, default: "",    null: false
    t.string   "phone",      limit: 255, default: "",    null: false
    t.text     "message",                                null: false
    t.text     "reply"
    t.string   "status",     limit: 255, default: "new", null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "jshare_carts", force: :cascade do |t|
    t.integer  "group_id",        null: false
    t.integer  "user_id",         null: false
    t.integer  "product_id",      null: false
    t.string   "product_package"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "jshare_groups", force: :cascade do |t|
    t.string   "group_name"
    t.string   "group_password"
    t.string   "email"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "jshare_users", force: :cascade do |t|
    t.integer  "group_id",   null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "billing"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "packages", force: :cascade do |t|
    t.string   "title",           limit: 255, default: "",         null: false
    t.text     "description",                                      null: false
    t.string   "hours_per_day",   limit: 255, default: "",         null: false
    t.string   "gb_storage",      limit: 255, default: "",         null: false
    t.string   "price_per_month", limit: 255, default: "",         null: false
    t.string   "status",          limit: 255, default: "inactive", null: false
    t.string   "premium",         limit: 255, default: "",         null: false
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.boolean  "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "title",       limit: 255, default: "",         null: false
    t.text     "description",                                  null: false
    t.string   "image",       limit: 255, default: "",         null: false
    t.string   "status",      limit: 255, default: "inactive", null: false
    t.string   "premium",     limit: 255, default: "",         null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "site_settings", force: :cascade do |t|
    t.string   "site_name",                        limit: 255,  default: "", null: false
    t.text     "footer_text",                                                null: false
    t.text     "footer_copyright",                                           null: false
    t.string   "contact_email",                    limit: 255,  default: "", null: false
    t.string   "contact_number",                   limit: 255,  default: "", null: false
    t.string   "gmap_longitude",                   limit: 255,  default: "", null: false
    t.string   "gmap_latitude",                    limit: 255,  default: "", null: false
    t.string   "logo",                             limit: 6000, default: "", null: false
    t.text     "service_page_title",                                         null: false
    t.text     "service_page_description",                                   null: false
    t.text     "service_page_opt_in_text",                                   null: false
    t.text     "how_work_page_title",                                        null: false
    t.text     "how_work_step3_title",                                       null: false
    t.text     "how_work_step2_title",                                       null: false
    t.text     "how_work_step1_title",                                       null: false
    t.text     "how_work_step3_description",                                 null: false
    t.text     "how_work_step2_description",                                 null: false
    t.text     "how_work_step1_description",                                 null: false
    t.text     "home_page_intro_title",                                      null: false
    t.text     "price_page_intro_description",                               null: false
    t.text     "home_page_features_title",                                   null: false
    t.text     "price_page_features_description",                            null: false
    t.text     "home_page_you_get_title",                                    null: false
    t.text     "price_page_you_get_description",                             null: false
    t.text     "home_page_start_now_title",                                  null: false
    t.text     "price_page_start_now_description",                           null: false
    t.text     "contact_address",                                            null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.text     "price_page_title",                                           null: false
    t.text     "price_page_description",                                     null: false
    t.string   "avatar",                           limit: 255
  end

  create_table "softwarecategories", force: :cascade do |t|
    t.string   "parent",     limit: 255,                      null: false
    t.string   "title",      limit: 255, default: "",         null: false
    t.string   "icon",       limit: 255, default: "",         null: false
    t.string   "status",     limit: 255, default: "inactive", null: false
    t.string   "image",      limit: 255, default: "",         null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  create_table "softwares", force: :cascade do |t|
    t.string   "cat_id"
    t.string   "sub_cat_id"
    t.string   "title"
    t.string   "description"
    t.string   "image"
    t.string   "isnew"
    t.string   "istop"
    t.string   "slider"
    t.string   "price_hourly"
    t.string   "price_monthly"
    t.string   "price_yearly"
    t.string   "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password"
    t.string   "phone"
    t.string   "country"
    t.string   "district"
    t.string   "image"
    t.string   "user_type"
    t.string   "status"
    t.datetime "login_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
