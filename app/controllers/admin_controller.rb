class AdminController < ApplicationController
    # before_action :set_product, only: [:show, :edit, :update, :destroy]

    layout false

    skip_before_action :verify_authenticity_token

    # GET /products
    # GET /products.json
    def index
        respond_to do |format|
            format.html { render :login, locals: { notice: '' } }
        end
    end

    # INQUERY
    #===========================================================================================================
    #===========================================================================================================
    # GET inquiry page
    def inquiry
        @inquirys = Inquiry.order(created_at: :desc)
        respond_to do |format|
            format.html { render :inquiry, locals: { notice: '' } }
        end
    end

    def inquiry_set
        @ss = Inquiry.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :inquiry_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def inquiry_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Inquiry reply sent successfully !!'
            Inquiry.where(id: params[:id]).first.update(reply: params[:reply],
                                                        status: 'replied')

        end
        @inquirys = Inquiry.order(created_at: :desc)
        respond_to do |format|
            format.html { render :inquiry, locals: {} }
        end
    end

    def inquiry_delete
        @inquiry = Inquiry.find_by(id: params[:id])
        @inquiry.destroy
        @inquirys = Inquiry.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Inquiry Deleted successfully !!'
        respond_to do |format|
            format.html { render :inquiry, locals: { notice: '' } }
        end
    end

    # PACKAGES
    #===========================================================================================================
    #===========================================================================================================
    # GET packages page
    def packages
        @packages = Package.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :packages, locals: { notice: '' } }
        end
    end

    # GET add page
    def packages_form
        @ss = Package.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :packages_form, locals: { notice: '' } }
        end
    end

    def packages_set
        @ss = Package.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :packages_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def packages_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Packages update successfully !!'
            Package.where(id: params[:id]).first.update(title: params[:title],
                                                        description: params[:description],
                                                        hours_per_day: params[:hours_per_day],
                                                        gb_storage: params[:gb_storage],
                                                        price_per_month: params[:price_per_month],
                                                        premium: params[:premium],
                                                        status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'Packages save successfully !!'
            package = Package.new
            package.title = params[:title]
            package.description = params[:description]
            package.hours_per_day = params[:hours_per_day]
            package.gb_storage = params[:gb_storage]
            package.price_per_month = params[:price_per_month]
            package.premium = params[:premium]
            package.status = params[:status]
            package.save
        end

        @packages = Package.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :packages, locals: {} }
        end
    end

    def packages_delete
        @package = Package.find_by(id: params[:id])
        @package.destroy
        @packages = Package.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Packages Deleted successfully !!'
        respond_to do |format|
            format.html { render :packages, locals: { notice: '' } }
        end
    end

    # SERVICES
    #===========================================================================================================
    #===========================================================================================================
    # GET service page
    def services
        @services = Service.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :services, locals: { notice: '' } }
        end
    end

    # GET add page
    def service_form
        @ss = Service.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :services_form, locals: { notice: '' } }
        end
    end

    def service_set
        @ss = Service.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :services_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def service_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Service update successfully !!'
            @service = Service.where(id: params[:id]).first
            if params[:image_file].present?
                begin
                    File.delete(File.join('public/assets', @service.image))
                rescue
                end
                if params[:image_file]
                    name = params[:image_file].original_filename.split('.')
                    file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                    File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }
                else
                    file_name = "No image"
                end


                @service.update(image: file_name)
            end
            @service.update(title: params[:title],
                            description: params[:description],
                            status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'Service save successfully !!'
            package = Service.new
            package.title = params[:title]
            package.description = params[:description]
            if params[:image_file]
                name = params[:image_file].original_filename.split('.')
                file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }
            else
                file_name = "No Image"

            end


            package.image = file_name
            package.status = params[:status]
            package.save
        end

        @services = Service.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :services, locals: {} }
        end
    end

    def service_delete
        @service = Service.find_by(id: params[:id])
        begin
            File.delete(File.join('public/assets', @service.image))
        rescue
        end
        @service.destroy
        @services = Service.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Service Deleted successfully !!'
        respond_to do |format|
            format.html { render :services, locals: { notice: '' } }
        end
    end

    # SuBSCRIBER
    #===========================================================================================================
    #===========================================================================================================
    # GET packages page
    def subscriber
        @subscribers = User.all.where(user_type: 'subscriber').order(created_at: :desc)
        respond_to do |format|
            format.html { render :subscriber, locals: { notice: '' } }
        end
    end

    def subscriber_set
        @ss = User.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :subscriber_form, locals: { notice: '' } }
        end
    end

    def subscriber_delete
        @subscribers = User.find_by(id: params[:id])
        @subscribers.destroy
        @subscribers = User.all.where(user_type: 'subscriber').order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Subscriber Deleted successfully !!'
        respond_to do |format|
            format.html { render :subscriber, locals: { notice: '' } }
        end
    end

    # POST add page
    def subscriber_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Subscriber update successfully !!'
            User.where(id: params[:id]).first.update(first_name: params[:first_name],
                                                     last_name: params[:last_name],
                                                     email: params[:email],
                                                     phone: params[:phone],
                                                     status: params[:status])

        end

        @subscribers = User.all.where(user_type: 'subscriber').order(created_at: :desc)
        respond_to do |format|
            format.html { render :subscriber, locals: {} }
        end
    end

    # HOMESLIDER
    #===========================================================================================================
    #===========================================================================================================
    # GET homeslider page
    def homeslider
        @homesliders = Homeslider.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :homeslider, locals: { notice: '' } }
        end
    end

    # GET add page
    def homeslider_form
        @ss = Homeslider.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :homeslider_form, locals: { notice: '' } }
        end
    end

    def homeslider_set
        @ss = Homeslider.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :homeslider_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def homeslider_save
        # raise

        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Slider update successfully !!'
            @slider = Homeslider.where(id: params[:id]).first

            if params[:image_file].present?
                begin
                    File.delete(File.join('public/assets', @slider.image))
                rescue
                end
                if params[:image_file]
                    name = params[:image_file].original_filename.split('.')
                    file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                    File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }
                else

                  file_name = "No image"
                end

                @slider.update(image: file_name)
            end

            @slider.update(title: params[:title],
                           tag_line: params[:tag_line],
                           description: params[:description],
                           read_more: params[:read_more],
                           start_now: params[:start_now],
                           price_per_month: params[:price_per_month],
                           status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'Slider save successfully !!'
            slider = Homeslider.new
            slider.title = params[:title]
            slider.tag_line = params[:tag_line]
            slider.description = params[:description]
            slider.read_more = params[:read_more]
            slider.start_now = params[:start_now]
            slider.price_per_month = params[:price_per_month]
           if params[:image_file]
               name = params[:image_file].original_filename.split('.')
               file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
               File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }
           else
             file_name = "No image"
           end

            slider.image = file_name
            slider.status = params[:status]
            slider.save
        end

        @homesliders = Homeslider.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :homeslider, locals: {} }
        end
    end

    def homeslider_delete
        @slider = Homeslider.find_by(id: params[:id])
        begin
            File.delete(File.join('public/assets', @slider.image))
        rescue
        end
        @slider.destroy
        @homesliders = Homeslider.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Slider Deleted successfully !!'
        respond_to do |format|
            format.html { render :homeslider, locals: { notice: '' } }
        end
    end

    # SOFTWARECATEGORY
    #===========================================================================================================
    #===========================================================================================================
    # GET softwarecategory page
    def softwarecategory
        @softwarecategorys = Softwarecategory.all.where(parent: '0').order(created_at: :desc)
        respond_to do |format|
            format.html { render :softwarecategory, locals: { notice: '' } }
        end
    end

    # GET add page
    def softwarecategory_form
        @ss = Softwarecategory.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :softwarecategory_form, locals: { notice: '' } }
        end
    end

    def softwarecategory_set
        @ss = Softwarecategory.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :softwarecategory_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def softwarecategory_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Category update successfully !!'
            Softwarecategory.where(id: params[:id]).first.update(parent: params[:parent],
                                                                 title: params[:title],
                                                                 icon: params[:icon],
                                                                 status: params[:status])
        else
            @msgls = 'alert alert-success'
            @msg = 'Category save successfully !!'
            slider = Softwarecategory.new
            slider.parent = 0
            slider.title = params[:title]
            slider.icon = params[:icon]
            slider.status = params[:status]
            slider.save
        end

        @softwarecategorys = Softwarecategory.all.where(parent: '0').order(created_at: :desc)
        respond_to do |format|
            format.html { render :softwarecategory, locals: {} }
        end
    end

    def softwarecategory_delete
        @slider = Softwarecategory.find_by(id: params[:id])
        @slider.destroy
        @softwarecategorys = Softwarecategory.all.where(parent: '0').order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Category Deleted successfully !!'
        respond_to do |format|
            format.html { render :softwarecategory, locals: { notice: '' } }
        end
    end

    # SOFTWARESUBCATEGORY
    #===========================================================================================================
    #===========================================================================================================
    # GET softwaresubcategory page
    def softwaresubcategory
        # @softwarecategorys = Softwarecategory.all.order(created_at: :desc)
        @softwaresubcategory = Softwarecategory.where.not(parent: '0').order(created_at: :desc)

        respond_to do |format|
            format.html { render :softwaresubcategory, locals: { notice: '' } }
        end
    end

    # GET add page
    def softwaresubcategory_form
        @ss = Softwarecategory.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :softwaresubcategory_form, locals: { notice: '' } }
        end
    end

    def softwaresubcategory_set
        @ss = Softwarecategory.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :softwaresubcategory_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def softwaresubcategory_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Sub Category update successfully !!'
            Softwarecategory.where(id: params[:id]).first.update(parent: params[:parent],
                                                                 title: params[:title],
                                                                 icon: params[:icon],
                                                                 status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'Sub Category save successfully !!'
            slider = Softwarecategory.new
            slider.parent = params[:parent]
            slider.title = params[:title]
            slider.icon = params[:icon]
            slider.status = params[:status]
            slider.save
        end

        @softwaresubcategory = Softwarecategory.all.where.not(parent: '0').order(created_at: :desc)
        respond_to do |format|
            format.html { render :softwaresubcategory, locals: {} }
        end
    end

    def softwaresubcategory_delete
        @slider = Softwarecategory.find_by(id: params[:id])
        @slider.destroy
        @softwaresubcategory = Softwarecategory.all.where.not(parent: '0').order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Category Deleted successfully !!'
        respond_to do |format|
            format.html { render :softwaresubcategory, locals: { notice: '' } }
        end
    end

    # SOFTWARE_N_GANES
    #===========================================================================================================
    #===========================================================================================================
    # GET software_n_games page

    def software_n_games
        @software_n_games = Software.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :software_n_games, locals: { notice: '' } }
        end
    end

    # GET add page
    def software_n_games_form
        @ss = Software.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :software_n_games_form, locals: { notice: '' } }
        end
    end

    def software_n_games_set
        @ss = Software.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :software_n_games_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def software_n_games_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Product update successfully !!'
            @software = Software.find(params[:id].to_i)
            if params[:image_file].present?
                begin
                    File.delete(File.join('public/assets', @software.image))
                rescue
                end
                if params[:image_file]
                    name = params[:image_file].original_filename.split('.')
                    file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                    File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }
                else
                    file_name = "No image"
                end


                @software.update(image: file_name)
            end
            @software.update(cat_id: params[:cat_id],
                             sub_cat_id: params[:sub_cat_id],
                             title: params[:title],
                             description: params[:description],
                             isnew: params[:isnew],
                             istop: params[:istop],
                             slider: params[:slider],
                             price_hourly: "#{params[:package_1_title]}, #{params[:package_1_hours]}, #{params[:package_1_unit_price]}",
                             price_monthly: "#{params[:package_2_title]}, #{params[:package_2_hours]}, #{params[:package_2_unit_price]}",
                             price_yearly: "#{params[:package_3_title]}, #{params[:package_3_hours]}, #{params[:package_3_unit_price]}",
                             status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'Product save successfully !!'
            slider = Software.new
            slider.cat_id = params[:cat_id]
            slider.sub_cat_id = params[:sub_cat_id]
            slider.title = params[:title]
            slider.description = params[:description]
            if params[:image_file]
                name = params[:image_file].original_filename.split('.')
                file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:image_file].read) }

            else
                file_name = "No image"
            end

            slider.image = file_name
            slider.isnew = params[:isnew]
            slider.istop = params[:istop]
            slider.slider = params[:slider]
            slider.price_hourly = "#{params[:package_1_title]}, #{params[:package_1_hours]}, #{params[:package_1_unit_price]}"
            slider.price_monthly = "#{params[:package_2_title]}, #{params[:package_2_hours]}, #{params[:package_2_unit_price]}"
            slider.price_yearly = "#{params[:package_3_title]}, #{params[:package_3_hours]}, #{params[:package_3_unit_price]}"
            slider.status = params[:status]
            slider.save
        end

        @software_n_games = Software.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :software_n_games, locals: {} }
        end
    end

    def software_n_games_delete
        @slider = Software.find_by(id: params[:id])
        begin
            File.delete(File.join('public/assets', @slider.image))
        rescue
        end
        @slider.destroy
        @software_n_games = Software.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Product Deleted successfully !!'
        respond_to do |format|
            format.html { render :software_n_games, locals: { notice: '' } }
        end
    end

    # SITE SETTING
    #===========================================================================================================
    #===========================================================================================================
    # GET site_setting page
    def site_setting_main
        @users = User.all
        @ss = Site_setting.find_by(id: 1)

        respond_to do |format|
            format.html { render :site_setting_main, locals: { notice: '' } }
        end
    end

    # POST update_site_setting page
    def update_site_setting_main
        @msgls = 'alert alert-success'
        @msg = 'Settings updated successfully !!'

        @setting = Site_setting.first

        if params[:logo].present?
            begin
                File.delete(File.join('public/assets', @setting.logo))
            rescue
            end

            name = params[:logo].original_filename.split('.')
            file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
            File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:logo].read) }

            @setting.update(logo: file_name)
        end

        @setting.update(site_name: params[:site_name],
                        footer_text: params[:footer_text],
                        footer_copyright: params[:footer_copyright],
                        contact_email: params[:contact_email],
                        contact_number: params[:contact_number],
                        gmap_longitude: params[:gmap_longitude],
                        gmap_latitude: params[:gmap_latitude],
                        contact_address: params[:contact_address],
                        avatar: params[:avatar])
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :site_setting_main, locals: {} }
        end
    end

    # GET site_setting page
    def site_setting_service
        @users = User.all
        @ss = Site_setting.find_by(id: 1)

        respond_to do |format|
            format.html { render :site_setting_service, locals: { notice: '' } }
        end
    end

    # POST update_site_setting page
    def update_site_setting_service
        @msgls = 'alert alert-success'
        @msg = 'Settings updated successfully !!'

        @setting = Site_setting.first

        @setting.update(service_page_title: params[:service_page_title],
                        service_page_description: params[:service_page_description],
                        service_page_opt_in_text: params[:service_page_opt_in_text])
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :site_setting_service, locals: {} }
        end
    end

    # GET site_setting page
    def site_setting_pricing
        @users = User.all
        @ss = Site_setting.find_by(id: 1)

        respond_to do |format|
            format.html { render :site_setting_pricing, locals: { notice: '' } }
        end
    end

    # POST update_site_setting page
    def update_site_setting_pricing
        @msgls = 'alert alert-success'
        @msg = 'Settings updated successfully !!'

        @setting = Site_setting.first

        @setting.update(price_page_title: params[:price_page_title],
                        price_page_description: params[:price_page_description])
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :site_setting_pricing, locals: {} }
        end
    end

    # GET site_setting page
    def site_setting_hiw
        @users = User.all
        @ss = Site_setting.find_by(id: 1)

        respond_to do |format|
            format.html { render :site_setting_hiw, locals: { notice: '' } }
        end
    end

    # POST update_site_setting page
    def update_site_setting_hiw
        @msgls = 'alert alert-success'
        @msg = 'Settings updated successfully !!'

        @setting = Site_setting.first

        @setting.update(how_work_page_title: params[:how_work_page_title],
                        how_work_step1_title: params[:how_work_step1_title],
                        how_work_step1_description: params[:how_work_step1_description],
                        how_work_step2_title: params[:how_work_step2_title],
                        how_work_step2_description: params[:how_work_step2_description],
                        how_work_step3_title: params[:how_work_step3_title],
                        how_work_step3_description: params[:how_work_step3_description])
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :site_setting_hiw, locals: {} }
        end
    end

    # GET site_setting page
    def site_setting_home
        @users = User.all
        @ss = Site_setting.find_by(id: 1)

        respond_to do |format|
            format.html { render :site_setting_home, locals: { notice: '' } }
        end
    end

    # POST update_site_setting page
    def update_site_setting_home
        @msgls = 'alert alert-success'
        @msg = 'Settings updated successfully !!'

        @setting = Site_setting.first

        @setting.update(home_page_intro_title: params[:home_page_intro_title],
                        price_page_intro_description: params[:price_page_intro_description],
                        home_page_features_title: params[:home_page_features_title],
                        price_page_features_description: params[:price_page_features_description],
                        home_page_you_get_title: params[:home_page_you_get_title],
                        price_page_you_get_description: params[:price_page_you_get_description],
                        home_page_start_now_title: params[:home_page_start_now_title],
                        price_page_start_now_description: params[:price_page_start_now_description])
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :site_setting_home, locals: {} }
        end
    end



    # CLOUD_SERVICESE
    #===========================================================================================================
    #===========================================================================================================
    # GET cloud_service page
    def cloud_service
        @cloud_services = Cloud_service.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_service, locals: { notice: '' } }
        end
    end

    # GET add page
    def cloud_service_form
        @ss = Cloud_service.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :cloud_service_form, locals: { notice: '' } }
        end
    end

    def cloud_service_set
        @ss = Cloud_service.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :cloud_service_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def cloud_service_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Cloud service update successfully !!'
            @cloud_service = Cloud_service.where(id: params[:id]).first
            if params[:logo].present?
                begin
                    File.delete(File.join('public/assets', @cloud_service.logo))
                rescue
                end

                name = params[:logo].original_filename.split('.')
                file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
                File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:logo].read) }

                @cloud_service.update(logo: file_name)
            end
            @cloud_service.update(title: params[:title],
                                  status: params[:status])
        else
            @msgls = 'alert alert-success'
            @msg = 'Cloud service save successfully !!'
            slider = Cloud_service.new
            slider.title = params[:title]

            name = params[:logo].original_filename.split('.')
            file_name = name[0] + '-' + SecureRandom.hex(5) + '.' + name[1]
            File.open(File.join('public/assets', file_name), 'wb') { |f| f.write(params[:logo].read) }

            slider.logo = file_name
            slider.status = params[:status]
            slider.save
        end

        @cloud_services = Cloud_service.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_service, locals: {} }
        end
    end

    def cloud_service_delete
        @slider = Cloud_service.find_by(id: params[:id])
        @slider.destroy
        @cloud_services = Cloud_service.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Cloud service Deleted successfully !!'
        respond_to do |format|
            format.html { render :cloud_service, locals: { notice: '' } }
        end
    end

    # CLOUD_OS
    #===========================================================================================================
    #===========================================================================================================
    # GET cloud_os page
    def cloud_os
        @cloud_oss = Cloud_os.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_os, locals: { notice: '' } }
        end
    end

    # GET add page
    def cloud_os_form
        @ss = Cloud_os.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :cloud_os_form, locals: { notice: '' } }
        end
    end

    def cloud_os_set
        @ss = Cloud_os.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :cloud_os_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def cloud_os_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Cloud os update successfully !!'
            Cloud_os.where(id: params[:id]).first.update(title: params[:title],
                                                         price: params[:price],
                                                         status: params[:status])
        else
            @msgls = 'alert alert-success'
            @msg = 'Cloud os save successfully !!'
            slider = Cloud_os.new
            slider.title = params[:title]
            slider.price = params[:price]
            slider.status = params[:status]
            slider.save
        end

        @cloud_oss = Cloud_os.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_os, locals: {} }
        end
    end

    def cloud_os_delete
        @slider = Cloud_os.find_by(id: params[:id])
        @slider.destroy
        @cloud_oss = Cloud_os.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Cloud os Deleted successfully !!'
        respond_to do |format|
            format.html { render :cloud_os, locals: { notice: '' } }
        end
    end

    # CLOUD_RAM
    #===========================================================================================================
    #===========================================================================================================
    # GET cloud_ram page
    def cloud_ram
        @cloud_rams = Cloud_ram.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_ram, locals: { notice: '' } }
        end
    end

    # GET add page
    def cloud_ram_form
        @ss = Cloud_ram.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :cloud_ram_form, locals: { notice: '' } }
        end
    end

    def cloud_ram_set
        @ss = Cloud_ram.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :cloud_ram_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def cloud_ram_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Cloud RAM update successfully !!'
            Cloud_ram.where(id: params[:id]).first.update(title: params[:title],
                                                          price: params[:price],
                                                          status: params[:status])
        else
            @msgls = 'alert alert-success'
            @msg = 'Cloud RAM save successfully !!'
            slider = Cloud_ram.new
            slider.title = params[:title]
            slider.title = params[:price]
            slider.status = params[:status]
            slider.save
        end

        @cloud_rams = Cloud_ram.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_ram, locals: {} }
        end
    end

    def cloud_ram_delete
        @slider = Cloud_ram.find_by(id: params[:id])
        @slider.destroy
        @cloud_rams = Cloud_ram.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Cloud RAM Deleted successfully !!'
        respond_to do |format|
            format.html { render :cloud_ram, locals: { notice: '' } }
        end
    end

    # CLOUD_HARDDISK
    #===========================================================================================================
    #===========================================================================================================
    # GET cloud_harddisk page
    def cloud_harddisk
        @cloud_harddisks = Cloud_harddisk.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_harddisk, locals: { notice: '' } }
        end
    end

    # GET add page
    def cloud_harddisk_form
        @ss = Cloud_harddisk.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :cloud_harddisk_form, locals: { notice: '' } }
        end
    end

    def cloud_harddisk_set
        @ss = Cloud_harddisk.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :cloud_harddisk_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def cloud_harddisk_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'Cloud HARD DISK update successfully !!'
            Cloud_harddisk.where(id: params[:id]).first.update(title: params[:title],
                                                               price: params[:price],
                                                               status: params[:status])
        else
            @msgls = 'alert alert-success'
            @msg = 'Cloud HARD DISK save successfully !!'
            slider = Cloud_harddisk.new
            slider.title = params[:title]
            slider.title = params[:price]
            slider.status = params[:status]
            slider.save
        end

        @cloud_harddisks = Cloud_harddisk.all.order(created_at: :desc)
        respond_to do |format|
            format.html { render :cloud_harddisk, locals: {} }
        end
    end

    def cloud_harddisk_delete
        @slider = Cloud_harddisk.find_by(id: params[:id])
        @slider.destroy
        @cloud_harddisks = Cloud_harddisk.all.order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'Cloud HARD DISK Deleted successfully !!'
        respond_to do |format|
            format.html { render :cloud_harddisk, locals: { notice: '' } }
        end
    end

    # ADMINUSER
    #===========================================================================================================
    #===========================================================================================================
    # GET adminuser page
    def adminuser
        @adminusers = User.all.where(user_type: 'admin').order(created_at: :desc)
        respond_to do |format|
            format.html { render :adminuser, locals: { notice: '' } }
        end
    end

    # GET add page
    def adminuser_form
        @ss = User.new
        @fromaction = 'Add'
        respond_to do |format|
            format.html { render :adminuser_form, locals: { notice: '' } }
        end
    end

    def adminuser_set
        @ss = User.find(params[:id])
        @fromaction = 'Edit'
        respond_to do |format|
            format.html { render :adminuser_form, locals: { notice: '' } }
        end
    end

    # POST add page
    def adminuser_save
        if params[:fromaction] == 'Edit'
            @msgls = 'alert alert-success'
            @msg = 'User update successfully !!'
            User.where(id: params[:id]).first.update(first_name: params[:first_name],
                                                     last_name: params[:last_name],
                                                     email: params[:email],
                                                     phone: params[:phone],
                                                     password: params[:password],
                                                     status: params[:status])

        else
            @msgls = 'alert alert-success'
            @msg = 'User save successfully !!'
            adminuser = User.new
            adminuser.first_name = params[:first_name]
            adminuser.last_name = params[:last_name]
            adminuser.email = params[:email]
            adminuser.phone = params[:phone]
            adminuser.password = params[:password]
            adminuser.status = params[:status]
            adminuser.user_type = 'admin'
            adminuser.save
        end

        @adminusers = User.all.where(user_type: 'admin').order(created_at: :desc)
        respond_to do |format|
            format.html { render :adminuser, locals: {} }
        end
    end

    def adminuser_delete
        @adminuser = User.find_by(id: params[:id])
        @adminuser.destroy
        @adminusers = User.all.where(user_type: 'admin').order(created_at: :desc)
        @msgls = 'alert alert-success'
        @msg = 'User Deleted successfully !!'
        respond_to do |format|
            format.html { render :adminuser, locals: { notice: '' } }
        end
    end

    # CMS
    def faq
        Cm.first.update(faq: params[:faq]) if params[:faq].present?

        @faq = Cm.first.faq
    end

    def release_notes
        if params[:release_notes].present?
            Cm.first.update(release_notes: params[:release_notes])
        end

        @release_notes = Cm.first.release_notes
    end

    def hiring
        Cm.first.update(hiring: params[:hiring]) if params[:hiring].present?

        @hiring = Cm.first.hiring
    end

    def product_support
        if params[:product_support].present?
            Cm.first.update(product_support: params[:product_support])
        end

        @product_support = Cm.first.product_support
    end

    def knowledge_base
        if params[:knowledge_base].present?
            Cm.first.update(knowledge_base: params[:knowledge_base])
        end

        @knowledge_base = Cm.first.knowledge_base
    end
end
