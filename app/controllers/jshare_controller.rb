class JshareController < ApplicationController
    protect_from_forgery :except => [:add_to_cart, :remove_from_cart, :change_billing, :get_sub_categories, :get_category_products, :get_sub_category_products, :search_products]

    def add_to_cart
        software = Software.find(params[:software_id])
        if JshareCart.where(group_id: params[:group_id], user_id: params[:user_id], product_id: software.id).blank?
            cart_item = JshareCart.create(group_id: params[:group_id], user_id: params[:user_id], product_id: software.id, product_package: software.price_hourly)
            render :text => cart_item.id.to_s + ',' + cart_item.software.title + ',' + params[:user_id]
        else
            render :text => false
        end
    end

    def remove_from_cart
        JshareCart.find(params[:cart_id]).destroy
        render :text => true
    end

    def change_billing
        JshareUser.find(params[:user_id]).update(billing: params[:billing])
        render :text => true
    end

    def get_sub_categories
        render text: Softwarecategory.where(parent: params[:category_id]).to_json
    end

    def get_category_products
        render text: Software.where(cat_id: params[:category_id]).to_json
    end

    def get_sub_category_products
        render text: Software.where(sub_cat_id: params[:sub_category_id]).to_json
    end

    def search_products
        render text: Software.where("title like ?", "%#{params[:search]}%").to_json
    end
end
