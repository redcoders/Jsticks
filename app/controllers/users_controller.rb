class UsersController < ApplicationController
    skip_before_action :verify_authenticity_token

    def index
        @ss = Site_setting.find_by(id: 1)
        @services = Service.where(status: 'active').limit(6)
        @homesliders = Homeslider.where(status: 'active')
    end

    # GET /products/new
    def new; end

    # GTT contact page
    def contact
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :contact, locals: { notice: '' } }
        end
    end

    def contact_send
        @ss = Site_setting.find_by(id: 1)
        @msgls = 'alert alert-success'
        @msg = 'Messsage send successfully, Thank you !!'
        inquiry = Inquiry.new
        inquiry.firstname = params[:firstname]
        inquiry.lastname = params[:lastname]
        inquiry.email = params[:email]
        inquiry.phone = params[:phone]
        inquiry.message = params[:message]
        inquiry.status = 'new'
        inquiry.save

        respond_to do |format|
            format.html { render :contact, locals: { notice: '' } }
        end
    end

    # INSERT INTO `site_settings` (`id`, `site_name`, `footer_text`, `footer_copyright`, `contact_email`, `contact_number`, `gmap_longitude`, `gmap_latitude`, `logo`, `service_page_title`, `service_page_description`, `service_page_opt_in_text`, `how_work_page_title`, `how_work_step3_title`, `how_work_step2_title`, `how_work_step1_title`, `how_work_step3_description`, `how_work_step2_description`, `how_work_step1_description`, `home_page_intro_title`, `price_page_intro_description`, `home_page_features_title`, `price_page_features_description`, `home_page_you_get_title`, `price_page_you_get_description`, `home_page_start_now_title`, `price_page_start_now_description`, `contact_address`, `created_at`, `updated_at`, `price_page_title`, `price_page_description`, `avatar`) VALUES
    # (1, 'jstick', 'This is footer about us autem vel eum iriure dolores in hendrerit in vulputate velit esse autem vel eum iriure dolore.', '© Copyright 2017 JSticks. All Right Reserved1', 'test@k.com', '9898098981', '41.901630', '12.460245', 'logo.png', 'Benefits Appreciate1', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'We give always awesome service.1', 'How It Works1', 'Finish iriure dolor3', 'Your actions via mouse, keyboard.2', 'Access JSticks Via Your Devices1', 'Duis autem vel eum iriure dolor3', 'Duis autem vel eum iriure dolor2', 'Duis autem vel eum iriure dolor1', 'What Is JSticks? 1', 'JSticks gives you a high performance gaming PC in the cloud. Access your SkyComputer from any device and play with ultra low latency and high performance graphics.1', 'Why Choose Us?1', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'What You Can Get1', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse1', 'We give always best cloud service for your use.2', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molesti consequat, vel illum dolore eu feugiat nulla faciisis at vero eros et accum dignissim qui blandit praesent luptatum.2', 'wages, Newada, USA', '2017-03-05 20:12:50', '2017-03-09 15:48:15', 'Choose Pricing Plans1', 'Duis islut aliquip ex ea commodo consequat. Duis autem1', NULL);

    # GTT how_work page
    def how_work
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :how_work, locals: { notice: '' } }
        end
    end

    # GTT price page
    def price
        @ss = Site_setting.find_by(id: 1)
        @packages = Package.where(status: 'active')
        respond_to do |format|
            format.html { render :price, locals: { notice: '' } }
        end
    end

    # GET service page
    def service
        @ss = Site_setting.find_by(id: 1)
        @services = Service.where(status: 'active')
        respond_to do |format|
            format.html { render :service, locals: { notice: '' } }
        end
    end

    # GET hardware_spec_jcreate page
    def hardware_spec_jcreate
        @ss = Site_setting.find_by(id: 1)
        @cloud_services = Cloud_service.where(status: 'active')
        @cloud_oss = Cloud_os.where(status: 'active')
        @cloud_rams = Cloud_ram.where(status: 'active')
        @cloud_harddisks = Cloud_harddisk.where(status: 'active')
        respond_to do |format|
            format.html { render :hardware_spec_jcreate, locals: { notice: '' } }
        end
    end

    # GET jstore page
    def jstore
        check_user_login

        @ss = Site_setting.find_by(id: 1)

        # if from fromjcreate page
        if params[:fromaction] == 'fromjcreate'
            session[:jcreate] = 'yes'
            session[:jcreate_customize_title] = params[:jcreate_customize_title]
            session[:jcreate_cloud_service] = params[:jcreate_cloud_service]
            session[:jcreate_cloud_os] = params[:jcreate_cloud_os]
            session[:jcreate_cloud_ram] = params[:jcreate_cloud_ram]
            session[:jcreate_cloud_harddisk] = params[:jcreate_cloud_harddisk]
        end

        # get all cats
        @categorys = Softwarecategory.all.where(parent: '0', status: 'active').order(title: :asc)

        # is any cat passed or not
        if params[:catid].present?
            @catid = params[:catid]
            @catdata = Softwarecategory.find(params[:catid])
        else
            @catdata = Softwarecategory.where(parent: '0', status: 'active').order(title: :desc).last
        end

        # get sub cats
        @subcategorys = Softwarecategory.all.where(parent: @catdata.id, status: 'active').order(title: :asc)

        # get softwares with if any sub cat passed
        if params[:subcatid].present?
            @subcatid = params[:subcatid]
            @softwares = Software.all.where(cat_id: @catdata.id, sub_cat_id: params[:subcatid], status: 'active').order(title: :asc)
        else
            @softwares = Software.all.where(cat_id: @catdata.id, status: 'active').order(title: :asc)
        end

        respond_to do |format|
            format.html { render :jstore, locals: { notice: '' } }
        end
    end

    # GET product page
    def product
        @ss = Site_setting.find_by(id: 1)

        @software = Software.find(params[:proid])
        @softwares = Software.all.where(cat_id: @software.cat_id, status: 'active').order(title: :asc)
        @catdata = Softwarecategory.find_by(id: @software.cat_id)
        @subcatdata = Softwarecategory.find_by(id: @software.sub_cat_id)

        respond_to do |format|
            format.html { render :product, locals: { notice: '' } }
        end
    end

    # GET cart page
    def cart
        @cart_items = Cart.where(user_id: session[:login_user_id])
        @cart_items = [] if @cart_items.blank?

        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :cart, locals: { notice: '' } }
        end
    end

    # GET cart page
    def remove_cart_item
        Cart.find(params[:id]).destroy

        redirect_to cart_path
    end

    # POST cart page
    def cartaction
        Cart.create(user_id: params[:user_id],
                    product_id: params[:product_id],
                    package: params[:choseplan])

        redirect_to cart_path
    end

    # GET select_user_jshare page
    def select_user_jshare
        if params[:group_id].present?
            @jshare_group = JshareGroup.find(params[:group_id])
        end

        @softwares = Software.all
        @categories = Softwarecategory.all.where(parent: 0)

        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :select_user_jshare, locals: { notice: '' } }
        end
    end

    def jshare_upload
        if params[:user_list].present?
            jshare_group = JshareGroup.new(group_name: params[:group_name], group_password: params[:group_password], email: params[:email])


            @workbook = Spreadsheet.open(params[:user_list].tempfile)
            @worksheet = @workbook.worksheet(0)
            1.upto @worksheet.last_row_index do |index|
                row = @worksheet.row(index)
                jshare_group.jshare_users << JshareUser.new(first_name: row[0], last_name: row[1], email: row[2], billing: 'self')
            end

            jshare_group.save

            redirect_to controller: 'users', action: 'select_user_jshare', group_id: jshare_group.id
        else
            redirect_to controller: 'users', action: 'select_user_jshare'
        end
    end

    # GET select_user_jshare page
    def billing
        if params[:group_id].present?
            @jshare_group = JshareGroup.find(params[:group_id])
        else
            redirect_to select_user_jshare_path
        end

        @ss = Site_setting.find_by(id: 1)
    end

    # GET select_user_jshare page
    def cart_jshare
        if params[:group_id].present?
            @jshare_group = JshareGroup.find(params[:group_id])
        else
            redirect_to select_user_jshare_path
        end

        @ss = Site_setting.find_by(id: 1)
    end

    # GET jsolo page
    def jsolo
        check_user_login

        @ss = Site_setting.find_by(id: 1)

        # get all cats
        @categorys = Softwarecategory.all.where(parent: '0', status: 'active').order(title: :asc)

        # is any cat passed or not
        if params[:catid].present?
            @catid = params[:catid]
            @catdata = Softwarecategory.find(params[:catid])
        else
            @catdata = Softwarecategory.where(parent: '0', status: 'active').order(title: :desc).last
        end

        # get sub cats
        @subcategorys = Softwarecategory.all.where(parent: @catdata.id, status: 'active').order(title: :asc)

        # get softwares with if any sub cat passed
        if params[:subcatid].present?
            @subcatid = params[:subcatid]
            @softwares = Software.all.where(cat_id: @catdata.id, sub_cat_id: params[:subcatid], status: 'active').order(title: :asc)
        else
            @softwares = Software.all.where(cat_id: @catdata.id, status: 'active').order(title: :asc)
        end

        respond_to do |format|
            format.html { render :jsolo, locals: { notice: '' } }
        end
    end

    # GET all_soft_jstore page
    def all_soft_jstore
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :all_soft_jstore, locals: { notice: '' } }
        end
    end

    # GET plan_jstore page
    def plan_jstore
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :plan_jstore, locals: { notice: '' } }
        end
    end

    # GET hardware_spec page
    def hardware_spec
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :hardware_spec, locals: { notice: '' } }
        end
    end

    # GET select_cloud page
    def select_cloud
        @ss = Site_setting.find_by(id: 1)
        respond_to do |format|
            format.html { render :select_cloud, locals: { notice: '' } }
        end
    end

    # CMS
    def faq
        @ss = Site_setting.find_by(id: 1)
        @faq = Cm.first.faq
    end

    def release_notes
        @ss = Site_setting.find_by(id: 1)
        @release_notes = Cm.first.release_notes
    end

    def hiring
        @ss = Site_setting.find_by(id: 1)
        @hiring = Cm.first.hiring
    end

    def product_support
        @ss = Site_setting.find_by(id: 1)
        @product_support = Cm.first.product_support
    end

    def knowledge_base
        @ss = Site_setting.find_by(id: 1)
        @knowledge_base = Cm.first.knowledge_base
    end

    private

    def check_user_login
        unless (session[:login_user].present?) || (session[:login_user].eql? "Yes")
            user = User.find(3)
            session[:login_user] = "yes"
            session[:login_user_id] = user.id
            session[:login_user_first_name] = user.first_name
            session[:login_user_last_name] = user.last_name
            session[:login_user_phone] = user.phone
        end
    end
end
