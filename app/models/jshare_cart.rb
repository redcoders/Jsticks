class JshareCart < ActiveRecord::Base
    belongs_to :jshare_user, primary_key: :user_id, foreign_key: :user_id, inverse_of: :jshare_carts

    belongs_to :jshare_group, primary_key: :group_id, foreign_key: :group_id, inverse_of: :jshare_carts

    has_one :software, primary_key: :product_id, foreign_key: :id, inverse_of: :jshare_cart
end
