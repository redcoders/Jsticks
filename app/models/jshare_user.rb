class JshareUser < ActiveRecord::Base
    belongs_to :jshare_group, primary_key: :group_id, foreign_key: :group_id, inverse_of: :jshare_users

    has_many :jshare_carts, foreign_key: :user_id, inverse_of: :jshare_user
end
