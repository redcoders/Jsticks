class Software < ActiveRecord::Base
    belongs_to :jshare_cart, foreign_key: :cart_id, inverse_of: :software
end
