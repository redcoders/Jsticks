class JshareGroup < ActiveRecord::Base
    has_many :jshare_users, foreign_key: :group_id, inverse_of: :jshare_group

    has_many :jshare_carts, foreign_key: :group_id, inverse_of: :jshare_group
end
